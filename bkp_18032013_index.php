<?php
session_start();
include("Mail.php");

if($_POST && !empty($_POST['nome_out'])) {

	if( $_SESSION["out"] != $_POST['nome_out'] ) {

		$recipients 				= 'demissoesrh@trip.com.br';
		
		$headers['From']    		= 'fabio@trip.com.br';
		$headers['To']      		= 'fabio@trip.com.br';
		$headers['Subject'] 		= 'Test message';
		$nome 						= 	$_POST['nome_out'];		
		$_SESSION["out"] 			=	$nome;

		//Parametros SMTP
		$params["host"] 			= "tripmail.trip.com.br";
		$params["port"] 			= "25"; 
		$params["auth"] 			= true; 
		$params["username"] 		= "fabio@trip.com.br"; 
		$params['password']			= 'password765';

		$smtp = Mail::factory('smtp', $params);	

		$titulo_email = array(	
							"Confirmar demissão", 
							"Retirar computador",
							"Retirar iPad",
							"Retirar celular",
							"Trocar senha de acesso ao e-mail e anti-spam",
							"Configurar resposta automática do e-mail",
							"Configurar encaminhamento do e-mail",
							"Agendar exclusão da conta do e-mail e anti-spam",
							"Bloquear acesso AD",
							"Bloquear acesso VPN",
							"Bloquear acesso Protheus",
							"Bloquear acesso BI",
							"Bloquear acesso sistema jurídico",
							"Bloquear acesso sistema chamados",
							"Bloquear acesso sistema banco de imagens",
							"Bloquear acesso sistema WoodWing",
							"Bloquear acesso sistema Mailing",
							"Bloquear acesso sistemas Config dos sites",
							"Bloquear acesso intranet",
							"Bloquear acesso dotproject",
							"Deletar cadastro na lista de ramais intranet",
							"Retirar ramal",
							"Retirar licença de softwares"
						);

		$titulos_qtd = count($titulo_email);

		$log = "<h2>Ações - E-mails enviados</h2><ol>";

		for( $i=0; $i < $titulos_qtd; $i++ ) {

			$headers['Subject'] 	= $nome." - ".$titulo_email[$i];
			$body				 	= "<p>{$nome}</p><p>".$titulo_email[$i]."</p>";
			
			$log .= "<li>".$headers['Subject']."</li>";
			
			$mail = $smtp->send($recipients, $headers, $body);
			//$mandou = mail( $emailto , $suject , $message , $headers );
		}
		$log .= "</ol>";

	}	else {

		$log = "
			<h2>Não Enviado</h2>
			<p><strong>Motivo:</strong> Você já enviou um e-mail com o nome ".$_POST['nome_out']."</p>
			<p>Caso queira reenviar, <a href=\"?acao=limpar\">clique aqui</a></p>
			";
	}

} 

if($_GET) {
	if( $_GET['acao'] == "limpar" ) {
		session_destroy();
	}
}

?>

<!doctype html>
<html lang="pt-br">	
	
	<head>		
		<title>Trip Out</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="_css/style.css">
		<script src="_js/jquery-1.9.0.js"></script>
		<script src="_js/functions.js"></script>
	</head>

	<body>
		<div id="container">
			<h1>OUT - Organization Underground Trip</h1>
			<?php if($log) { ?>

			<div id="box_form">								
				<?php echo $log; ?>				
			</div>
			<div class="box_texto">
				<img src="_img/chrome-rocks.png" style="margin-bottom: 20px;" />
				<a href="index.php" class="voltar">Voltar</a>
			</div>

			<?php } else { ?>

			<div id="box_form">
				<form method="post" action="?" id="form_out" >
					<label>Nome do funcionário</label>
					<input type="text" name="nome_out" value="" placeholder="Nome do funcionário"/>		
					<button type="submit">Enviar</button>
				</form>	
			</div>
			<div class="box_texto">Digite o nome da pessoa a ser desligada!</div>
			
			<?php } ?>
		</div>
	</body>

</html>