<?php
ini_set('display_errors',1);
session_start();
//include("Mail.php");
require "PHPMailer/PHPMailerAutoload.php";

if($_GET) {
	if( $_GET['acao'] == "limpar" ) { unset($_SESSION['out']); }
	if( $_GET['acao'] == "limparlast" ) { unset($_SESSION['last']); }
}

if( $_POST ) {
	if($_POST['senha'] == "TR1P53RV3R!") {		

		if( (!empty($_POST['nome_out'])) && (!empty($_POST['tipo_envio'])) ) {

			if( $_SESSION["out"] != $_POST['nome_out'] ) {

				$recipients 				= 'demissoesrh@trip.com.br';
				//$recipients 				= 'testechamados@trip.com.br';
				
				$headers['From']    		= 'fabio@trip.com.br';
				$headers['To']      		= 'fabio@trip.com.br';
				$headers['MIME-Version']	= '1.0';
				$headers["Content-type"]	= "text/html; charset=UTF-8";
				$headers['Subject'] 		= 'Test message';
				$nomes 						= 	$_POST['nome_out'];		
				$_SESSION["out"] 			=	$nomes;
				$_SESSION["last"]			=	$nomes;
				$tipo_envio					= 	$_POST['tipo_envio'];

				//Parametros SMTP
				// $params["host"] 			= "tripmail.trip.com.br";
				// $params["port"] 			= "25"; 
				// $params["auth"] 			= true; 
				$params["username"] 		= "fabio@trip.com.br"; 
 				$params['password']			= 'password765';

				//$smtp = Mail::factory('smtp', $params);	

				$mail = new PHPMailer();
				$mail->isSMTP();
				//$mail->SMTPDebug 		= 2;
				//$mail->Debugoutput 	= 'html';
				$mail->Host 		= "tripmail.trip.com.br";
				$mail->Port 		= 587;
				$mail->SMTPAuth 	= true;
				//$mail->isHTML(true);
				$mail->Username 	= $params["username"];
				$mail->Password 	= $params['password'];
				$mail->setFrom($headers['From'], 'Suporte');
				//$mail->addReplyTo('cael@trip.com.br', 'Desenvolvedor');
				//$mail->addAddress('cael@trip.com.br', 'Desenvolvedor');


				//////////////////////////////////////////////////////////
				//// MENSAGENS PARA DEMISSAO
				/////////////////////////////////////////////////////////

				/****************************************
				//SUPORTE TI
				*****************************************/
				
				$depts['d']['suporte_ti']['dep']		= "Suporte TI";
				$depts['d']['suporte_ti']['destino'] 	= "suporte@trip.com.br";
				$depts['d']['suporte_ti']['assuntos']	=	array(
														"computador"		=>	"- Retirar computador",
														"ramal"				=>	"- Retirar ramal",
														"iPad"				=>	"- Retirar iPad",
														"e-mail"			=>	"- Trocar senha de acesso ao e-mail <br />
																				 - Configurar resposta autom&aacute;tica do e-mail <br />
																				 - Configurar encaminhamento do e-mail <br />
																				 - Agendar exclus&atilde;o da conta do e-mail e anti-spam.",
														"AD"				=>	"- Bloquear acesso AD",
														"VPN"				=>	"- Bloquear acesso VPN",
														"Chamados"			=>	"- Bloquear acesso sistema chamados",
														"ID CANON"			=>	"- Deletar acesso sistema ID CANON",
														"Software"			=>	"- Retirar licen&ccedil;a de softwares",
														"Mac Address"		=>	"- Apagar cadastro Mac Adress",
														"Celular"			=>	"- Retirar celular"
													);

				/****************************************
				//RH
				*****************************************/
				$depts['d']['rh']['dep']				=	"RH[Recursos Humanos]";
				$depts['d']['rh']['destino']			=	"rh@trip.com.br";
				$depts['d']['rh']['assuntos']			= 	array(
																"Ramais"	=>	"- Deletar cadastro na lista de ramais intranet"
															);

				/////////////////////////////////////////////////////////
				//// FIM MENSAGENS PARA CONFIRMAR
				/////////////////////////////////////////////////////////

				/*****************************************
				// Enviando
				*****************************************/
				$log = "<h2>Ações - E-mails enviados</h2>";

				$nomes = explode(",",$nomes);
				
				//verifica qual array irá enviar
				if( $tipo_envio == "confirmar" ) 	{ $deptos = $depts['c']; }
				if( $tipo_envio == "demitir" ) 		{ $deptos = $depts['d']; }

				foreach($nomes as $nome) {
					//manipulando a variavel nome recebida
					// ECHO $nome;
					$firstNome = explode(" ", $nome);
					$nome_email["aka"] = trim($firstNome[0]);
					$nome = explode("-", $nome);
					$nome_email["nome_comp"] = trim($nome[0]);
					$nome_email["nome_depto"] = trim($nome[1]);
					

					if(trim($nome_email['aka'])) {
						$log .="<h2>".$nome_email['aka']."</h2><hr>";
						foreach( $deptos as $dept ) {
							$recipients	= $dept['destino'];
							$log .= "<h3>".$dept['dep']."</h3><ol>";

							foreach( $dept['assuntos'] as $assunto => $corpo ) {
								$headers['Subject'] = 	"[D] ".$nome_email['aka']." - ".$assunto;
								$body				=	$nome_email['nome_comp']." - ". $nome_email['nome_depto'] ."<br /><br />".$corpo;
								$log .= "<li>".$headers['Subject']."</li>";

								$mail->addAddress($recipients, $nome_email['aka']);
								$mail->Subject 		= $headers['Subject'];

								$mail->msgHTML($body);
								//$mail->msgHTML(file_get_contents('examples/contents.html'), dirname(__FILE__));

								if (!$mail->send()) {
								    echo "Mailer Error: " . $mail->ErrorInfo;
								} else {
								    $log .= "<p>enviado</p>";;
								}								

								//$mail = $smtp->send($recipients, $headers, $body);

								//if($mail) { $log .= "<p>enviado</p>"; }
							}
							$log .= "</ol>";
						}		
						
					}					
				}
				//exit();

				

			}	else {

				$log = "
					<h2>Não Enviado</h2>
					<p><strong>Motivo:</strong> Você já enviou um e-mail com o nome ".$_POST['nome_out']."</p>
					<p>Caso queira reenviar, <a href=\"?acao=limpar\">clique aqui</a></p>
					";
			}

		} else {

			$erro = "<p class=\"erro\">Necessário informar um nome e tipo de envio.</p>";
		}
		

	} else {

		$erro = "<p class=\"erro\">Senha incorreta!</p>";

	}
}


?>

<!doctype html>
<html lang="pt-br">	
	
	<head>		
		<title>Trip Out</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="_css/style.css">
		<script src="_js/jquery-1.9.0.js"></script>
		<script src="_js/functions.js"></script>
	</head>

	<body>
		<div id="container">
			<h1>OUT - Organization Underground Trip</h1>
			<?php if(isset($log)){ ?>

			<div id="box_form">								
				<?php echo $log; ?>				
			</div>
			<div class="box_texto">
				<div class="mascara">
					<?php
						// $fontFace['out'] = explode(" ", $_SESSION['out']);
					?>
					<span class="mask"><?php echo $nome_email["aka"]; ?> está <br /><strong>demitido</strong></span>
					<img src="_img/out.jpg" style="margin-bottom: 20px;" />
				</div>
				<a href="index.php" class="voltar">Demitir mais alguém</a>
			</div>

			<?php } else { ?>

			<div id="box_form">
				<form method="post" action="?" id="form_out" >
					<textarea name="nome_out" value="" placeholder="Nome - Departamento"></textarea>
					<input type="password" name="senha" placeholder="Senha" />
					<label for="tipo_envio">Tipo de envio:</label>
					<select name="tipo_envio">
						<option value="">--Escolha a opção--</option>
						<option value="confirmar">CONFIRMAR</option>
						<option value="demitir">DEMITIR</option>
					</select>
					<button type="submit">Enviar</button>
				</form>
			</div>
			<div class="box_texto">Digite o nome da pessoa a ser desligada!<?php echo isset($erro); ?>
				<? if(isset($_SESSION['last'])){?>
				<h2>Ultimos Enviados</h2>
				<textarea style="width:100%"><?php echo $_SESSION['last']; ?></textarea>
				<a href="?acao=limparlast">Limpar nomes</a>
				<?}?>

				<div class="notepad" style="width:100%">
					<button id="limpar">limpar</button>
					<h2>Bloco de notas</h2>
					<div id="notepad" contenteditable="true">
					</div>
				</div>
			</div>			
			
			<?php } ?>
		</div>
	</body>

</html>
