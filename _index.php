<?php
session_start();
include("Mail.php");

if( $_POST ) {
	if($_POST['senha'] == "TR1P53RV3R!" ) {

		if(!empty($_POST['nome_out'])) {

			if( $_SESSION["out"] != $_POST['nome_out'] ) {

				$recipients 				= 'demissoesrh@trip.com.br';
				
				$headers['From']    		= 'fabio@trip.com.br';
				$headers['To']      		= 'fabio@trip.com.br';
				$headers['MIME-Version']	= '1.0';
				$headers["Content-type"]	= "text/html; charset=UTF-8";
				$headers['Subject'] 		= 'Test message';
				$nome 						= 	$_POST['nome_out'];		
				$_SESSION["out"] 			=	$nome;

				//Parametros SMTP
				$params["host"] 			= "tripmail.trip.com.br";
				$params["port"] 			= "25"; 
				$params["auth"] 			= true; 
				$params["username"] 		= "fabio@trip.com.br"; 
				$params['password']			= 'password765';

				$smtp = Mail::factory('smtp', $params);	

				/****************************************
				//SUPORTE TI
				*****************************************/
				$depts['suporte_ti']['dep']			= "Suporte TI";
				$depts['suporte_ti']['destino'] 	= "suporte@trip.com.br";
				$depts['suporte_ti']['assuntos']	=	array(
														"computador"		=>	"- Retirar computador",
														"ramal"				=>	"- Retirar ramal",
														"iPad"				=>	"- Retirar iPad",
														"e-mail"			=>	"- Trocar senha de acesso ao e-mail <br />
																				 - Configurar resposta automática do e-mail <br />
																				 - Configurar encaminhamento do e-mail <br />
																				 - Agendar exclusão da conta do e-mail e anti-spam.",
														"AD"				=>	"- Bloquear acesso AD",
														"VPN"				=>	"- Bloquear acesso VPN",
														"Chamados"			=>	"- Bloquear acesso sistema chamados",
														"Banco de imagens"	=>	"- Deletar acesso sistema banco de imagens",
														"WoodWing"			=>	"- Deletar acesso sistema WoodWing",
														"Software"			=>	"- Retirar licença de softwares",
														"Mac Address"		=>	"- Apagar cadastro Mac Adress"
													);

				/****************************************
				//SUPORTE TRANSPORTES
				*****************************************/
				$depts['transportes']['dep']		 = "Transportes";
				$depts['transportes']['destino']	 = "transportes@trip.com.br";
				$depts['transportes']['assuntos']	 =	array( 
														"Celular"			=>	"- Retirar celular"
														);

				/****************************************
				//SUPORTE PROTHEUS
				*****************************************/
				$depts['protheus']['dep']		=	"Suporte Protheus";
				$depts['protheus']['destino']	=	"suporteprotheus@trip.com.br";
				$depts['protheus']['assuntos']	=	array(
														"Protheus"			=>	"- Deletar acesso Protheus",
														"BI"				=>	"- Deletar acesso BI",
														"Jurídico"			=>	"- Deletar acesso sistema jurídico"
													);

				/****************************************
				//DESENVOLVIMENTO WEB
				*****************************************/
				$depts['dev_web']['dep']			=	"Desenvolvimento Web";
				$depts['dev_web']['destino']		=	"desenvolvimentoweb@trip.com.br";
				$depts['dev_web']['assuntos']		=	array(
														"Sistemas"			=>	"Deletar acesso sistemas: <br />
																				 - Project <br />
																				 - Intranet <br />
																				 - CMS - Trip <br />
																				 - CMS - Tpm <br />
																				 - CMS - Transformadores <br />
																				 - CMS - Institucional <br />
																				 - CMS - Mailing <br />
																				 - CMS - Assinaturas"
													);

				/****************************************
				//RH
				*****************************************/
				$depts['rh']['dep']				=	"RH[Recursos Humanos]";
				$depts['rh']['destino']			=	"rh@trip.com.br";
				$depts['rh']['assuntos']		= 	array(
														"Ramais"			=>	"- Deletar cadastro na lista de ramais intranet"
													);


				/*****************************************
				// Enviando
				*****************************************/
				$log = "<h2>Ações - E-mails enviados</h2>";

				foreach( $depts as $dept ) {
					$recipients	= $dept['destino'];
					$log .= "<h3>".$dept['dep']."</h3><ol>";

					foreach( $dept['assuntos'] as $assunto => $corpo ) {
						$headers['Subject'] = 	"[D] ".$nome." - ".$assunto;
						$body				=	"{$nome}<br /><br />".$corpo;
						$log .= "<li>".$headers['Subject']."</li>";

						$mail = $smtp->send($recipients, $headers, $body);
					}
					$log .= "</ol>";
				}
						
				

			}	else {

				$log = "
					<h2>Não Enviado</h2>
					<p><strong>Motivo:</strong> Você já enviou um e-mail com o nome ".$_POST['nome_out']."</p>
					<p>Caso queira reenviar, <a href=\"?acao=limpar\">clique aqui</a></p>
					";
			}

		} else {

			$erro = "<p class=\"erro\">Necessário informar um nome.</p>";
		}

		if($_GET) {
			if( $_GET['acao'] == "limpar" ) {
				session_destroy();
			}
		}

	} else {

		$erro = "<p class=\"erro\">Senha incorreta!</p>";

	}
}

?>

<!doctype html>
<html lang="pt-br">	
	
	<head>		
		<title>Trip Out</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="_css/style.css">
		<script src="_js/jquery-1.9.0.js"></script>
		<script src="_js/functions.js"></script>
	</head>

	<body>
		<div id="container">
			<h1>OUT - Organization Underground Trip</h1>
			<?php if($log) { ?>

			<div id="box_form">								
				<?php echo $log; ?>				
			</div>
			<div class="box_texto">
				<div class="mascara">
					<?php
						$besteirou = explode(" ", $nome);
					?>
					<span class="mask"><?php echo $besteirou[0]; ?> está <br /><strong>demitido</strong></span>
					<img src="_img/out.jpg" style="margin-bottom: 20px;" />
				</div>
				<a href="index.php" class="voltar">Demitir mais alguém</a>
			</div>

			<?php } else { ?>

			<div id="box_form">
				<form method="post" action="?" id="form_out" >
					<input type="text" name="nome_out" value="" placeholder="Nome do funcionário" />
					<input type="password" name="senha" placeholder="Senha" />
					<button type="submit">[ X ] DEMITIR</button>
				</form>	
			</div>
			<div class="box_texto">Digite o nome da pessoa a ser desligada!<?php echo $erro; ?></div>

			
			<?php } ?>
		</div>
	</body>

</html>